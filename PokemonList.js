'use strict';
const Pokemon = require('./Pokemon');

class PokemonList extends Array {

  add(...args) {
    this.push(new Pokemon(...args));
  };

  show() {
    //const pokemons = this.map(item => `${item.show()}`).join(', ');
    //console.log('Pokemons : %s of %d persons', pokemons, this.length);
    console.log(`Pokemons list of ${this.length} persons`);

    for(let pokemon of this) {
      pokemon.show();
    }
  };

  max() {
    const maxLevel = Math.max(...this);

    return this.find( item => maxLevel == item.level);
  };

  transfer(pokemonName, destination) {
    const indexTargetPokemon = this.findIndex( item => {
      return item.name == pokemonName;
    });
    if(indexTargetPokemon == -1) {
      alert(`Pokemon ${pokemonName} is absent!!!
Transfer weren't done!!!`);
      return;
    }
    const targetPokemon = this.splice(indexTargetPokemon, 1);
    destination.push(...targetPokemon);
  }
}

module.exports = PokemonList;
