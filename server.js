'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
let db; // сохраняю переменную базы данных в глобальной области для доступа из функций модуля
const port = process.env.PORT || 3000;


// раздача статики через express
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
  db = require('./db.js'); // подключили db - абстрактную базу данных
  // подключение БД в middleware, чтобы каждое новое подключение создавало свой экземпляр БД
  // очистка кэша https://habrahabr.ru/post/233827/
  delete require.cache[require.resolve('./db.js')];

  next();
});
app.use('/user/:id', function (req, res, next) { // у предварительной middleware - 3 параметра
              // у middleware, обрабатывающей ошибки и расположенной ниже роутов - 4 параметра
  const id = req.params.id;

  if(id >= db.length) {
    // res.status(400).send({error: `User ${req.params.id} hasn't existed yet`}); // в middleware такой синтаксис не используется
    const err = new Error(`User ${req.params.id} hasn't existed yet`);
    err.status = 404;
    throw err;
  }

  next();
});

app.get('/users/', function (req, res) {
  const offset = +req.query.offset;
  const limit = +req.query.limit;
  const field = +req.query.field;
  let arr = db;

  if(offset) {
    if(limit) {
      arr = db.slice(offset, (limit + offset));
    } else {
      arr = db.slice(offset);
    }
  } else {
    if(limit) {
      arr = db.slice(0, limit);
    } else {
      arr = db.slice();
    }
  }

  res.send(arr);
});

app.get('/user/:id', function (req, res) {
  res.send(formatResponse(db[req.params.id], (req.params.id)));
});

app.post('/users/', function (req, res) {

  if(!(isFinite(req.body.score)) || !(typeof req.body.name === 'string')) {
    // !(typeof req.body.name === 'string') - избыточное условие, т.к. в элементе пользовательского ввода input - тип string.
    // Использую для целей тестирования
    const err = new Error('Bad request!');
    err.status = 400;
    throw err;
  }

  db.push({
    name: req.body.name,
    score: req.body.score
  });

  res.send(formatResponse(db[db.length - 1], (db.length - 1)));
});

app.put('/user/:id', function (req, res) {
  db[req.params.id].name = req.body.name;
  db[req.params.id].score = req.body.score;
  //console.log(req.body.name);
  res.send(formatResponse(db[req.params.id], req.params.id));
});

app.delete('/user/:id', function (req, res) {
  if(req.headers['delete-all']) {
    db = [];
    res.send(db);
    return;
  }

  const output = db[req.params.id];
  delete db[req.params.id];  // удаление без смещения индекса (id)
  res.send(formatResponse(output, req.params.id));
});


app.use(function (err, req, res, next) {
  //console.log(err);   // выводится объект [Error: User 12 hasn't existed yet]
  //res.status(err.status).send(err.stack); // Uncaught SyntaxError: Unexpected token E in JSON at position 0
  res.status(err.status).json(err.stack);  // правильно преобразует инфо об ошибке в json
});

app.listen(port, function () {
  console.log('Start HTTP server on %d port', port);
});

function formatResponse(user, id) {
  const output = {
    user,
    id
  };
  return output;
}