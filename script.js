'use strict';
// Enter point (NodeJS)
const Pokemon = require('./Pokemon.js');
const PokemonList = require('./PokemonList');


const found = new PokemonList(
  new Pokemon('Tauros', 1), new Pokemon('Pinsir', 2), new Pokemon('Jynx', 3)
);
const lost = new PokemonList(
  new Pokemon('Porygon', 4), new Pokemon('Kabuto', 6), new Pokemon('Zapdos', 5)
);

lost.add('Grisha', 44);
lost.add('Hatabich', 12);
found.add('Ditto', 7);
console.log('------found-----------');
found.show();

console.log('------lost-----------');
lost.show();

// lost.transfer('Kabuto1', found); // имя покемона на правильное для проверки
lost.transfer('Kabuto', found);
console.log('-------found + 1----------');
found.show();

console.log('-------max found----------');
console.log(found.max());