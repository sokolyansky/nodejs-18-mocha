'use strict';

const expect = require('chai').expect;
const Pokemon = require('../Pokemon.js');

describe('Pokemon', () => {
  describe('Pokemon.show()', () => {
    var str;

    beforeEach(() => {
      const log = global.console.log;
      global.console.log = function (message) {
        str = message;
      };

      const pokemon = new Pokemon('Test', 1);
      pokemon.show();

      global.console.log = log;
    });


    it('outputs full entry about pokemon to the console', () => {

      // Как протестировать текст сообщения, выведенный на экран, ведь данный метод ничего не возвращает?
      // Предлагаю создать вспомогательный абстрактный класс, в котором перед началом тестирования происходит
      // перенаправление стандартного потока вывода, а после тестирования всё возвращается на изначальные позиции.

      //console.log(1).pipe(output);  // как перенаправить поток вывода?
      expect(str).to.equal('This is Test has level 1');
    });
  });

});
