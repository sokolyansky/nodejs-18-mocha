const supertest = require('supertest');
const expect = require('chai').expect;


describe('Test REST API server', () => {
  let server, userID;

  beforeEach((done) => {
    require('../server');
    //delete require.cache[require.resolve('../server')]; // очищать cache в server.js
    server = supertest.agent('http://localhost:3000');

    done();
  });

  it('creates user', (done) => {
    const user = {name: 'Alex', score: 44};

    server
      .post('/users')
      .send(user)
      .expect(200)
      .end((error, response) => {

        expect(error).is.null;
        expect(response.body.user).to.eql(user);
        //console.log(response.body);
        userID = response.body.id;

        done();
      });
  });

  it('doesn\'t create user with invalid fields', (done) => {
    const wrongUser = {name: 888, score: 'Ok'};

    server
      .post('/users')
      .send(wrongUser)
      .expect(400, done);
  });

  it('removes user by ID', (done) => {
    const currentUser = {
      user: {name: 'Александр Малов', score: 60},
      id: 7
    };
    server
      .delete(`/user/${currentUser.id}`)
      .expect(200)
      .end((error, response) => {
        expect(error).is.null;
        expect(response.body.user).to.eql(currentUser.user);

        done();
      });
  });

  it('removes not existing user is impossible', (done) => {
    const wrongUserID = 8;
    server
      .delete(`/user/${wrongUserID}`)
      .expect(404)
      .end(done);
  });
});
