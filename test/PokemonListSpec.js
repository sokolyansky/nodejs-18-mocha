'use strict';

const expect = require('chai').expect;
const PokemonList = require('../PokemonList');
const Pokemon = require('../Pokemon');

describe('PokemonList', () => {
  describe('PokemonList.add()', () => {

    let pokemonList;

    it('creates an instance of PokemonList which is an Array', () => {
      pokemonList = new PokemonList();
      pokemonList.add('Test1', 10);

      expect(Array.isArray(pokemonList)).to.equal(true);
      expect(pokemonList[0] instanceof Pokemon).to.equal(true);
    });

    it('returns a proper object when adding 1 pokemon', () => {
      pokemonList = new PokemonList();
      pokemonList.add('Test1', 10);

      expect(pokemonList[0]).to.eql({name: 'Test1', level: 10});
    });

    it('returns a proper object when not adding any pokemon', () => {
      pokemonList = new PokemonList();
      pokemonList.add();

      expect(pokemonList[0]).to.eql({name: 'Anonym', level: 0});
    });
  });


  describe('PokemonList.max()', () => {
    let pokemonList;


    it('selects pokemon with max level from list of 3 pokemons', () => {
      pokemonList = new PokemonList();

      [
        ['Test1', 10], ['Test2', 5], ['Test3', 0]
      ].forEach((pokemon) => {
        pokemonList.add(...pokemon);
      });

      expect(pokemonList.max()).to.eql({name: 'Test1', level: 10});
    });

    it('selects pokemon with max level from empty list', () => {
      pokemonList = new PokemonList();

      expect(pokemonList.max()).to.equal(undefined);
    });
  });


  describe('PokemonList.show()', () => {
    let str = '';

    before(() => {
      let log;
      log = global.console.log;

      const pokemonList = new PokemonList();
      pokemonList.add('Test1', 10);

      global.console.log = function (message) {
        str += message + ' ';
      };
      pokemonList.show();
      global.console.log = log;
    });

    it('outputs info about PokemonList content to the console', () => {

      expect(str.trim()).to.equal(`Pokemons list of 1 persons This is Test1 has level 10`);
    })
  });

});