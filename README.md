# test-server-by Mocha
Demo files for Netology Node, Angular, MongoDB Course. Lesson 18

1. npm install
2. npm test

Part I Test a Pokemon (Unit tests) (PokemonSpec, PokemonListSpec)
-  script.js - the entry point (NodeJS)
Files for tests: Pokemon.js, PokemonList.js,

Part II Test a REST API server (Integration tests)  (serverRESTSpec)
-  server.js - the entry point (NodeJS)
Files for tests: db.js