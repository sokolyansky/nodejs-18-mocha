class Pokemon {
  constructor (name = 'Anonym', level = 0) {
    this.name = name;
    this.level = level;
  }

  show() {
    console.log(`This is ${this.name} has level ${this.level}`);
  };
  valueOf() {
    return this.level;
  }
}

module.exports = Pokemon;